//
//  mainViewController.swift
//  Project
//
//  Created by Karim HAMDI on 03/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT
import AWSMobileClient




class mainViewController: UIViewController {
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var mqttlabel: UILabel!
    
    @IBOutlet weak var freeLabel: UILabel!
    @IBOutlet weak var occupiedLabel: UILabel!
    
    
    var presence: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.freeLabel.text = "Now"
        self.occupiedLabel.text = ""
        
        // Do any additional setup after loading the view.
       // sensorsTempShadowUpdate()
        sensorsTempShadowUpdate()
        getOccupation()
        getPlantWater()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sensorsTempShadowUpdate()
        getOccupation()
    }
    
    
    func getPlantWater (){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        
        iotDataManager.subscribe(toTopic: "$aws/things/PlantIot/shadow/update/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    
                    guard let state = dictio["state"] as? [String: Any] else {return}
                   guard let desired = state["reported"] as? [String: Any] else {return}
                    guard let thirsty = desired["thirsty"] as? Int else {return}
                    
                    if (thirsty == 1){
                        let alert = UIAlertController(title: "Plante IoT", message: "La plante a soif, veuillez lui donner de l'eau :)!", preferredStyle: UIAlertController.Style.alert)
                        let twoAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
                        let oneAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
                        
                        alert.addAction(oneAction)
                        alert.addAction(twoAction)
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                } catch {
                    print(error)
                }
            }
        })
            
    }
    
    func getOccupation (){
        
         let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        
        iotDataManager.subscribe(toTopic: "roomf110/features/presence", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            if (!self.presence) {
                self.presence = true
                print("Precence détected: \(stringValue)")
                DispatchQueue.main.async { // Correct
                    self.freeLabel.text = ""
                    self.occupiedLabel.text = "Now"
                }
                
                
                self.turnLight(boo: true)
                self.alerte(str: "Une présence vient d'être détectée.")
            }
            else {
                self.presence = false
                self.alerte(str: "Reste-t-il des personnes dans la salle?")
            }
        } )
    }
    
    func alerte(str: String) {
        let alert = UIAlertController(title: "Détection", message: str, preferredStyle: UIAlertController.Style.alert)
        
        var oneAction: UIAlertAction
        
        var twoAction: UIAlertAction
        
        if (self.presence) {
            twoAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
            oneAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        }
        else {
            oneAction = UIAlertAction(title: "Non", style: UIAlertAction.Style.cancel, handler: { action in
                DispatchQueue.main.async { // Correct
                    self.freeLabel.text = "Now"
                    self.occupiedLabel.text = ""
                }
                self.turnLight(boo: false)
            })
            twoAction = UIAlertAction(title: "Oui", style: UIAlertAction.Style.default, handler: { action in
                self.presence = true
            })
        }
       
        
       
        alert.addAction(oneAction)
        alert.addAction(twoAction)

        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func popUpView() {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpVc") as! PopUpViewController
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    
    func turnLight(boo: Bool){
        
        var lights : [String: Any] = [:]
        
        for i in 1..<7 {
            lights[String(i)] = [
                "state" : [
                    "on" : boo
                ]
            ]
        }
    
        let dico = [
            "state" : [
                "desired" : [
                    "lights" : lights
                ]
            ]
        ]
        
        shadowUpdate(dic: dico)
    
    }

  
    
    func sensorsTempShadowUpdate () {
        sensorsTempShadowGetSubscribe()
        sensorsTempShadowGetPublish()
    }
    
    func sensorsTempShadowGetPublish() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "$aws/things/GrovePiSensors/shadow/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func sensorsTempShadowGetSubscribe() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "$aws/things/GrovePiSensors/shadow/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    guard let state = dictio["state"] as? [String: Any] else {return}
                    guard let reported = state["reported"] as? [String: Any] else {return}
                    
                    guard let temperature = reported["temperature"] as? String else {return}
                    
                    self.tempLabel.text = temperature + " °C"
                    
                    guard let humidity = reported["humidity"] as? String else {return}
                    
                    self.humidityLabel.text = humidity + " %"
                    
                    
                    
                } catch {
                    print(error)
                }
                
            }
        } )
        
        
        
        iotDataManager.subscribe(toTopic: "$aws/things/GrovePiSensors/shadow/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    func getUnit(unit: String) -> String {
        switch(unit) {
        case "celcius":
                return "°C"
        case "fahrenheit":
                return "°F"
        case "kalvin":
                return "°K"
        default:
            return ""
        }
    }
   
    @IBAction func driveAction() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        iotDataManager.publishString("", onTopic: "roomf110/features/drive/show", qoS: .messageDeliveryAttemptedAtMostOnce)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}
