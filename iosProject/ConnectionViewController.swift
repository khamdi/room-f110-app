//
//  ConnectionViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 07/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT
import AWSMobileClient
import SQLite

class ConnectionViewController: UIViewController {
    
    var connected = false;
    var first = true
    
    var iotDataManager: AWSIoTDataManager!;
    var iotManager: AWSIoTManager!;
    var iot: AWSIoT!
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
    }
    
    func createDB () {
        do {
            let documentDirectory = try
                FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("groupes").appendingPathExtension("sqlite3")
            let base = try Connection(fileUrl.path)
            db = base
        }
        catch {
            print(error)
        }
        
        createTableDB()
    }
    
    func createTableDB() {
        createLightTable()
        createGroupeTable()
        createGroupeLightTable()
    }
    
    func createLightTable() {
        print ("CreateLightTable Début")
        
        let dropTable = light_Table.drop(ifExists: true)
        
        let createTable = light_Table.create{ table in
            table.column(light_id, primaryKey: true)
            table.column(light_name)
            
        }
        do {
            try db.run(dropTable)
            try db.run(createTable)
        } catch {
            print(error)
        }
        print ("CreateTableLigt Fin")
    }
    
    func createGroupeTable() {
        print ("CreateGroupeTable Début")
        
        let dropTable = groupe_Table.drop(ifExists: true)
        
        let createTable = groupe_Table.create{ table in
            table.column(groupe_id, primaryKey: true)
            table.column(groupe_name)
            
        }
        do {
            try db.run(dropTable)
            try db.run(createTable)
        } catch {
            print(error)
        }
        print ("CreateTableGroupe Fin")
    }
    
    func createGroupeLightTable() {
        print ("CreateGroupeLightTable Début")
        
        let dropTable = groupeLights_Table.drop(ifExists: true)
        
        let createTable = groupeLights_Table.create{ table in
            table.column(groupeLights_id_groupe)
            table.column(groupeLights_id_light)
            table.primaryKey(groupeLights_id_groupe, groupeLights_id_light)
            table.foreignKey(groupeLights_id_groupe, references: groupe_Table, groupe_id)
            table.foreignKey(groupeLights_id_light, references: light_Table, light_id)
            
        }
        do {
            try db.run(dropTable)
            try db.run(createTable)
        } catch {
            print(error)
        }
        print ("CreateTableGroupeLigt Fin")
    }
    
    
    public func connection() {
        if (first) {
            AWSMobileClient.sharedInstance().initialize { (userState, error) in
                guard error == nil else {
                    print("Failed to initialize AWSMobileClient. Error: \(error!.localizedDescription)")
                    return
                }
                print("AWSMobileClient initialized.")
            }
            
            let iotEndPoint = AWSEndpoint(urlString: IOT_ENDPOINT);
            
            let iotConfiguration = AWSServiceConfiguration(region: AWSRegion, credentialsProvider: AWSMobileClient.sharedInstance())
            
            let iotDataConfiguration = AWSServiceConfiguration(region: AWSRegion, endpoint: iotEndPoint, credentialsProvider: AWSMobileClient.sharedInstance())
            
            
            AWSServiceManager.default().defaultServiceConfiguration = iotConfiguration
            iotManager = AWSIoTManager.default()
            iot = AWSIoT.default()
            
            AWSIoTDataManager.register(with: iotDataConfiguration!, forKey: ASWIoTDataManager)
            iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
            
            self.connect()
        }
        else {
            self.connect()
        }
    }
    
    func connect() {
        
        func mqttEventCallback( _ status: AWSIoTMQTTStatus )
        {
            DispatchQueue.main.async {
                print("connection status = \(status.rawValue)")
                switch(status)
                {
                case .connecting:
                    
                    print( "Connecting.. " )
                    
                case .connected:
                    print( "Connected" )
                    
                    self.connected = true
                    // sender.isEnabled = true
                    let uuid = UUID().uuidString;
                    let defaults = UserDefaults.standard
                    let certificateId = defaults.string( forKey: "certificateId")
                    
                    print("Using certificate:\n\(certificateId!)\n\n\nClient ID:\n\(uuid)")
                    
                    
                case .disconnected:
                    print( "Disconnected" )
                    
                case .connectionRefused:
                    print( "Connection Refused")
                    
                case .connectionError:
                    print( "Connection Error" )
                    
                case .protocolError:
                    print( "Protocole Error" )
                    
                default:
                    print("unknown state: \(status.rawValue)")
                    
                }
                
                NotificationCenter.default.post( name: Notification.Name(rawValue: "connectionStatusChanged"), object: self )
            }
        }
        
        if (connected == false ) {
            let defaults = UserDefaults.standard
            var certificateId = defaults.string(forKey: "certificateId")
            //certificateId = nil
            if (certificateId == nil) {
                DispatchQueue.main.async {
                    print("No identity found in bundle, creating one...")
                }
                
                // Now create and store the certificate ID in NSUserDefaults
                let csrDictionary = [ "commonName":CertificateSigningRequestCommonName, "countryName":CertificateSigningRequestCountryName, "organizationName":CertificateSigningRequestOrganizationName, "organizationalUnitName":CertificateSigningRequestOrganizationalUnitName ]
                
                self.iotManager.createKeysAndCertificate(fromCsr: csrDictionary, callback: {  (response ) -> Void in
                    if (response != nil)
                    {
                        defaults.set(response?.certificateId, forKey:"certificateId")
                        defaults.set(response?.certificateArn, forKey:"certificateArn")
                        certificateId = response?.certificateId
                        print("response: [\(String(describing: response))]")
                        
                        let attachPrincipalPolicyRequest = AWSIoTAttachPrincipalPolicyRequest()
                        attachPrincipalPolicyRequest?.policyName = PolicyName
                        attachPrincipalPolicyRequest?.principal = response?.certificateArn
                        
                        // Attach the policy to the certificate
                        self.iot.attachPrincipalPolicy(attachPrincipalPolicyRequest!).continueWith (block: { (task) -> AnyObject? in
                            if let error = task.error {
                                print("failed: [\(error)]")
                            }
                            print("result: [\(String(describing: task.result))]")
                            
                            // Connect to the AWS IoT platform
                            if (task.error == nil)
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                    print("Using certificate: \(certificateId!)")
                                    let uuid = UUID().uuidString
                                    self.iotDataManager.connect( withClientId: uuid, cleanSession:true, certificateId:certificateId!, statusCallback: mqttEventCallback(_:))
                                    self.connected = true
                                    
                                })
                            }
                            return nil
                        })
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            //sender.isEnabled = true
                            //self.activityIndicatorView.stopAnimating()
                            print("Unable to create keys and/or certificate, check values in Constants.swift")
                        }
                    }
                } )
            }
            else{
                let uuid = UUID().uuidString;
                
                // Connect to the AWS IoT service
                iotDataManager.connect( withClientId: uuid, cleanSession:true, certificateId:certificateId!, statusCallback: mqttEventCallback(_:))
                connected = true
            }
        }
        else {
            print( "Disconnecting...")
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                self.iotDataManager.disconnect();
                DispatchQueue.main.async {
                    self.connected = false
                }
            }
        }
    }
    
    
}

func shadowUpdate(dic: [String: Any]){
    let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
    print(JSONSerialization.isValidJSONObject(dic))
    
    do {
        let jsonDatas = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        let jsonString = try String(data: jsonDatas, encoding: String.Encoding.utf8)
        
        print("Shadow Update : \(jsonString)")
        
        iotDataManager.publishString(jsonString!, onTopic: "$aws/things/PhilipsHueBridge/shadow/update", qoS:.messageDeliveryAttemptedAtMostOnce)
    }catch {
        print(error)
    }
}
