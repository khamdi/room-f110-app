//
//  CreateGroupViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 18/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT
import SQLite


class CreateGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    //var first: Bool = false

    
    var bridge: Bridge = Bridge.init(name: "", lights: [])
    var index: [Int] = []
    
    let identifiantModuleCellule = "lampsid"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.hideKeyboardWhenTappedAround()
        bridgeShadowUpdate()


    }
    
    
    @IBAction func textFieldAction() {
        if self.nameTF.text == "" {
            self.saveButton.isEnabled = false
        }
        else {
            self.saveButton.isEnabled = true
        }
    }
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bridge.lights.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifiantModuleCellule)
        }
        
        cell!.textLabel?.text = "\(bridge.lights[indexPath.row].name)"
        
        //cell?.accessoryType = .checkmark
        if (self.bridge.lights[indexPath.row].color){
            let str = "On : \(bridge.lights[indexPath.row].on), Brigthness: \(bridge.lights[indexPath.row].bri), Color: \(bridge.lights[indexPath.row].xy)"
            print (str)
            cell!.detailTextLabel?.text = str
        }
        else if (self.bridge.lights[indexPath.row].reachable){
            let str = "On : \(bridge.lights[indexPath.row].on), Brigthness: \(bridge.lights[indexPath.row].bri))"
            print (str)
            cell!.detailTextLabel?.text = str
        }

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected the cell, \(indexPath.row)")
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                self.index = self.index.filter{ $0 != indexPath.row}
            }
            else{
                cell.accessoryType = .checkmark
                self.index.append(indexPath.row)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Bridge : \(bridge.name)"
    }
    
    @IBAction func saveAction() {
        var res : [String] = []
        print(index)
        
        for i in 0..<self.index.count {
            
            res.append(self.bridge.lights[self.index[i]].id)
        }
        
        let dic = [
            "name" :  nameTF.text,
            "lightIds" : res
            ] as [String : Any]
        
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        do {
            let jsonDatas = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            
            let jsonString = String(data: jsonDatas, encoding: String.Encoding.utf8)
            
            print(jsonString!)
            
            iotDataManager.publishString(jsonString!, onTopic: "things/PhilipsHueBridge/features/groups/new", qoS:.messageDeliveryAttemptedAtMostOnce)
            
        }catch {
            print(error)
        }
    }
    
    
    func bridgeShadowUpdate () {
        bridgeShadowGetSubscribe()
        bridgeShadowGetPublish()
    }
    
    func bridgeShadowGetPublish() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "$aws/things/PhilipsHueBridge/shadow/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func bridgeShadowGetSubscribe() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    guard let state = dictio["state"] as? [String: Any] else {return}
                    guard let delta = state["reported"] as? [String: Any] else {return}
                    
                    guard let bridgeName = delta["name"] as? String else {return}
                    
                    guard let lightsArray = delta["lights"] as? [String: Any] else {return}
                    
                    var lightss: [Lights] = []
                    
                    for  i in lightsArray { //Max lamp on a bridge
                       // guard let id = lightsArray[String(i)] as? [String: Any] else {break}
                        
                        guard let id = i.value as? [String: Any] else {break}
                        
                        guard let lightName = id["name"] as? String else {return}
                        guard let lightType = id["type"] as? String else {return}
                        guard let lightState = id["state"] as? [String: Any] else {return}
                        guard let lighton = lightState["on"] as? Int else {return}
                        guard let bri = lightState["brightness"] as? Int else {return}
                        guard let lightReachable = lightState["reachable"] as? Int else {return}
                        guard let lightColorTemp = lightState["colorTemp"] as? Int else {return}
                        
                        if (lightType == "Extended color light" ) {
                            guard let xy = lightState["xy"] as?  [Double] else {return}
                            guard let lightEffect = lightState["effect"] as? String else {return}
                            
                            lightss.append(Lights.init(id: i.key, name: lightName, bri: bri, on: lighton, xy: (xy[0], xy[1]), color: lightType,  reachable: lightReachable, colorTemp: lightColorTemp, effect: lightEffect))
                        }else{
                            lightss.append(Lights.init(id: i.key, name: lightName, bri: bri, on: lighton, color: lightType,  reachable: lightReachable, colorTemp: lightColorTemp))
                        }
                        
                    }
                    
                    print("lights : \(lightss.count)")
                    self.bridge = Bridge.init(name: bridgeName, lights: lightss)
                    
                    self.tableView.reloadData()
                    print(self.bridge.lights.count)
                    print(self.bridge.name )
                } catch {
                    print(error)
                }
                
            }
        } )
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
