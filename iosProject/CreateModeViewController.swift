//
//  CreateModeViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 18/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT

class CreateModeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    var bridge: Bridge = Bridge.init(name: "", lights: [])

    let identifiantModuleCellule = "groupsid"
    
    var action:[String:Any] = [:]
    var groups: [(String, String, [String])] = []
    var selectedID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bridgeShadowUpdate()
        groupsUpdate()
        
        self.hideKeyboardWhenTappedAround() 
        
      //  self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: Selector(“endEditing:”)))
        
        
    }
    

    
    @IBAction func textFieldChange() {
        if self.nameTF.text == "" {
            self.saveButton.isEnabled = false
        }
        else {
            self.saveButton.isEnabled = true
        }
    }
    
    func getType(b: Bool) -> String {
        if (b){
            return "Extended color light"
        }
        return "None"
    }
    
    func getEffect(b:Bool) -> String {
        print("b: \(b )" )
        if (b == true){
            return "colorloop"
        }
        return "none"
    }
    
    @IBAction func saveAction() {
        
        let dic = [
            "name" : self.nameTF.text,
            "lightIds": self.groups[self.selectedID].2,
            ] as [String : Any]
        
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        
        do {
            let jsonDatas = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            let jsonString = try String(data: jsonDatas, encoding: String.Encoding.utf8)
            
            print(jsonString)
            
            iotDataManager.publishString(jsonString!, onTopic: "things/PhilipsHueBridge/features/scenes/new", qoS:.messageDeliveryAttemptedAtMostOnce)
        }catch {
            print(error)
        }
        
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifiantModuleCellule)
        }
        cell!.textLabel?.text = "\(groups[indexPath.row].1)"
        
        //cell?.accessoryType = .checkmark
        var str = "Lights :"
        for i in 0..<self.groups[indexPath.row].2.count {
            str += " \(self.groups[indexPath.row].2[i]) -"
        }
        print (str)
        cell!.detailTextLabel?.text = String(str.dropLast())
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
        self.saveButton.isEnabled = true
        self.selectedID = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        self.saveButton.isEnabled = false
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Light Group:"
    }
    
    func groupsUpdate() {
        groupsGetSubscribe()
        groupsGetPublish()
    }
    
    func groupsGetPublish(){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "things/PhilipsHueBridge/features/groups/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func groupsGetSubscribe(){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/groups/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    
                    for i in dictio {
                        guard let id = i.value as? [String: Any] else {return}
                        guard let name = id["name"] as? String else {return}
                        guard let lightIds = id["lightIds"] as? [String] else {return}
                        self.groups.append((i.key, name, lightIds))
                    }
                    
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
            }
        })
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/groups/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    func bridgeShadowUpdate () {
        bridgeShadowGetSubscribe()
        bridgeShadowGetPublish()
    }
    
    func bridgeShadowGetPublish() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "$aws/things/PhilipsHueBridge/shadow/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func bridgeShadowGetSubscribe() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    guard let state = dictio["state"] as? [String: Any] else {return}
                    guard let delta = state["reported"] as? [String: Any] else {return}
                    
                    guard let bridgeName = delta["name"] as? String else {return}
                    
                    guard let lightsArray = delta["lights"] as? [String: Any] else {return}
                    
                    var lightss: [Lights] = []
                    
                    for  i in lightsArray { //Max lamp on a bridge
                        // guard let id = lightsArray[String(i)] as? [String: Any] else {break}
                        
                        guard let id = i.value as? [String: Any] else {break}
                        guard let lightName = id["name"] as? String else {return}
                        guard let lightType = id["type"] as? String else {return}
                        guard let lightState = id["state"] as? [String: Any] else {return}
                        guard let lighton = lightState["on"] as? Int else {return}
                        guard let bri = lightState["brightness"] as? Int else {return}
                        guard let lightReachable = lightState["reachable"] as? Int else {return}
                        
                        guard let xy = lightState["xy"] as?  [Double] else {return}
                        guard let lightColorTemp = lightState["colorTemp"] as? Int else {return}
                        guard let lightEffect = lightState["effect"] as? String else {return}
                        
                        lightss.append(Lights.init(id: i.key, name: lightName, bri: bri, on: lighton, xy: (xy[0], xy[1]), color: lightType,  reachable: lightReachable, colorTemp: lightColorTemp, effect: lightEffect))
                        
                        
                    }
                    
                    self.bridge = Bridge.init(name: bridgeName, lights: lightss)
                    
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
                
            }
        } )
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
