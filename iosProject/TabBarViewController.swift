//
//  TabBarViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 07/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        
         ConnectionViewController().connection()
        
        ConnectionViewController().createDB()
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
