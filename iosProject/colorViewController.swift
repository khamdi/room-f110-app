//
//  ViewController.swift
//  SwiftHSVColorPicker
//
//  Created by johankasperi on 2015-08-20.
//

import Foundation
import UIKit
import SwiftHSVColorPicker

class colorViewController: UIViewController {
    
    // Init ColorPicker with yellow
    var selectedColor: UIColor = UIColor.white

    @IBOutlet weak var colorPicker: SwiftHSVColorPicker!
    
    var nbLight: [String]!
    var xy: (Double, Double)!
    

    
    // IBOutlet for the ColorPicker
        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Setup Color Picker
        self.colorPicker.setViewColor(selectedColor)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getSelectedColor(_ sender: UIButton) {
        let point = self.colorPicker.colorWheel.point
        var x: Double, y: Double
        
        print("x: \(((point?.x)!)/255), y: \((point?.y)!/255)")
        
        print(UIColor.init(cgColor: colorPicker.color.cgColor))
        
        if (self.colorPicker.colorWheel.color == whiteC) {
            print("Blanc")
            x = XWHITE
            y = YWHITE
        } else {
            self.xy = ( Double((point?.x)!/280), Double((point?.y)!/280) )
            x = ( (10000 * self.xy!.0).rounded() ) / 10000
            y = ( (10000 * self.xy!.1).rounded() ) / 10000
        }
        
        var tmp: [String: Any] = [:]
        
        for i in self.nbLight {
            tmp[i] = [
                "state" : [
                    "xy" : [
                        x,
                        y
                    ]
                ]
            ]
        }
        
        tmp.reversed()
        
        let dic = [
            "state" : [
                "desired" : [
                    //"name" : self.bridge.name,
                    "lights" : tmp
                ]
            ]
        ]
        
        print(dic)
        shadowUpdate(dic: dic)
    }
}
