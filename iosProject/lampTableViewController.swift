//
//  lampTableViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 07/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT

class lampTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lampSwitch: UISwitch!
    @IBOutlet weak var brightSlider: UISlider!
    @IBOutlet weak var colorLoopButton: UIButton!
    
    let identifiantModuleCellule = "lampid"
    
    @IBOutlet weak var tableView: UITableView!
    
    let MAXNBLAMP = 12
    
    var bridge : Bridge = Bridge.init(name: "", lights: [])
    var rBridge : Bridge!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bridge.lights.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = self.tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule, for: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifiantModuleCellule)
        }
        cell!.textLabel?.text = "\(bridge.lights[indexPath.row].name)"
        
        if (self.bridge.lights[indexPath.row].reachable && self.bridge.lights[indexPath.row].color){
            cell?.accessoryType = .detailButton
            let str = "On : \(bridge.lights[indexPath.row].on), Brigthness: \(bridge.lights[indexPath.row].bri), Color: \(bridge.lights[indexPath.row].xy)"
           print (str)
            cell!.detailTextLabel?.text = str
        }
        else if (self.bridge.lights[indexPath.row].reachable){
            let str = "On : \(bridge.lights[indexPath.row].on), Brigthness: \(bridge.lights[indexPath.row].bri))"
            print (str)
            cell!.detailTextLabel?.text = str
        }
        else {
            cell!.backgroundColor = UIColor.lightGray
            cell!.detailTextLabel?.text = "Not Reachable"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("You selected the cell, \(indexPath.row)")

    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print(indexPath.row)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let singleLampViewController = storyBoard.instantiateViewController(withIdentifier: "SingleLampViewController") as! SingleLampViewController
        singleLampViewController.indeX = self.bridge.lights[indexPath.row].id
        singleLampViewController.isOn = self.bridge.lights[indexPath.row].on
        singleLampViewController.bri = Float(self.bridge.lights[indexPath.row].bri)
        singleLampViewController.xy = self.bridge.lights[indexPath.row].xy
        singleLampViewController.lampName = "\(self.bridge.lights[indexPath.row].name)"
        self.navigationController?.pushViewController(singleLampViewController, animated: true)
    }
   
   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Bridge : \(bridge.name)"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
         print("test")
        //Update the parameter Bridge from AWS with the Shadow of the bridge
        bridgeShadowUpdate()
        
       // self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: self.identifiantModuleCellule)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func getReachableLampIndice() -> [Int] {
        var res: [Int] = []
        
        for i in 0..<self.bridge.lights.count {
            if (self.bridge.lights[i].reachable){
                res.append(i)
            }
        }
        return res
    }
 
    func getReachableColorLampIndice() -> [String] {
        var res: [String] = []
        
        for i in 0..<self.bridge.lights.count {
            if (self.bridge.lights[i].reachable && self.bridge.lights[i].color){
                res.append(self.bridge.lights[i].id)
            }
        }
        return res
    }
    
    @IBAction func colorButton() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let colorViewController = storyBoard.instantiateViewController(withIdentifier: "colorViewController") as! colorViewController
        
        colorViewController.nbLight = getReachableColorLampIndice()
        self.navigationController?.pushViewController(colorViewController, animated: true)
        
        //print(colorViewController.nbLight)
    }
    
    
    func bridgeShadowUpdate () {
        bridgeShadowGetSubscribe()
        bridgeShadowGetPublish()
    }
    
    func bridgeShadowGetPublish() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "$aws/things/PhilipsHueBridge/shadow/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func bridgeShadowGetSubscribe() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    guard let state = dictio["state"] as? [String: Any] else {return}
                    guard let delta = state["reported"] as? [String: Any] else {return}
                    
                    guard let bridgeName = delta["name"] as? String else {return}
                    
                    guard let lightsArray = delta["lights"] as? [String: Any] else {return}
                    
                    var lightss: [Lights] = []
                    
                    for  i in lightsArray { //Max lamp on a bridge
                        guard let id = i.value as? [String: Any] else {break}
                        
                            guard let lightName = id["name"] as? String else {return}
                            guard let lightType = id["type"] as? String else {return}
                            guard let lightState = id["state"] as? [String: Any] else {return}
                            guard let lighton = lightState["on"] as? Int else {return}
                            guard let bri = lightState["brightness"] as? Int else {return}
                            guard let lightReachable = lightState["reachable"] as? Int else {return}
                            guard let lightColorTemp = lightState["colorTemp"] as? Int else {return}

                        if (lightType == "Extended color light" ) {
                            guard let xy = lightState["xy"] as?  [Double] else {return}
                            guard let lightEffect = lightState["effect"] as? String else {return}
                            
                            lightss.append(Lights.init(id: i.key, name: lightName, bri: bri, on: lighton, xy: (xy[0], xy[1]), color: lightType,  reachable: lightReachable, colorTemp: lightColorTemp, effect: lightEffect))
                        }else{
                            lightss.append(Lights.init(id: i.key, name: lightName, bri: bri, on: lighton, color: lightType,  reachable: lightReachable, colorTemp: lightColorTemp))
                        }

                 
                    }
                    
                    print("lights : \(lightss)")
                    self.bridge = Bridge.init(name: bridgeName, lights: lightss)
                    self.rBridge = self.getReachableBridge()
                    if (self.bridge.lights.count != 0){
                        for i in 0..<self.bridge.lights.count {
                            if (self.bridge.lights[i].on) {
                                self.lampSwitch.isOn = self.bridge.lights[i].on
                                self.brightSlider.value = Float(self.bridge.lights[i].bri)
                                break
                            }
                            else {
                                self.brightSlider.isEnabled = false
                            }
                        }
                    }
                    
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
                
            }
        } )
        
        iotDataManager.subscribe(toTopic: "$aws/things/PhilipsHueBridge/shadow/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    static func getOn (nb: Int) -> Bool{
        if (nb == 0) {
            return false
        }
        return true
    }
    
    func getReachableBridge() -> Bridge {
        var res: [Lights] = []
        
        for i in 0..<self.bridge.lights.count {
            if (self.bridge.lights[i].reachable) {
                res.append(self.bridge.lights[i])
            }
        }
        return  Bridge(name: self.bridge.name, lights: res)
    }

    // Change
    func changeStateBridge() {
        for i in 0..<rBridge.lights.count {
            self.rBridge.lights[i].on = self.lampSwitch.isOn
        }
    }
    
    @IBAction func switchAction() {
        
        if (self.lampSwitch.isOn){
            self.brightSlider.isEnabled = true
            self.brightSlider.value = Float(self.rBridge.lights[0].bri)
        }
        else {
            self.brightSlider.isEnabled = false
            self.brightSlider.value = 0
        }
        
        
       changeStateBridge()
        
        var ligts : [String: Any] = [:]
        
        for i in 0..<self.bridge.lights.count {
            ligts[self.bridge.lights[i].id] = [
                "state" : [
                    "on" : self.bridge.lights[i].on
                ]
            ]
        }
        
        
        let dic = [
            "state" : [
                "desired" : [
                    "lights" :  ligts
                ]
            ]
        ]
        
        shadowUpdate(dic: dic)

    }
    
    
    func changeBriBridge() {
        for i in 0..<rBridge.lights.count {
            rBridge.lights[i].bri = Int(brightSlider.value)
        }
    }
    
    func getEffect(b:Bool) -> String {
        print("b: \(b )" )
        if (b == true){
            return "colorloop"
        }
        return "none"
    }
    
    
    
    @IBAction func colorLoopEffect() {
        self.colorLoopButton.isSelected = !self.colorLoopButton.isSelected
        
        var res : [String: Any] = [:]
        var tmp : [String: Any] = [:]
        
        let rBridge = getReachableBridge()
        for i in 0..<rBridge.lights.count {
            print(self.colorLoopButton.isSelected)
            print(getEffect(b: self.colorLoopButton.isSelected))
            
            if (rBridge.lights[i].color){
                tmp = [
                    "state": [
                        "effect": getEffect(b: self.colorLoopButton.isSelected)
                        ]
                ]
                res[rBridge.lights[i].id] = tmp
            }
        }
        
        let dico = [
            "state" : [
                "desired" : [
                    "lights" : res
                ]
            ]
        ]
        
        print(dico)
        shadowUpdate(dic: dico)
    }
    
    
    @IBAction func brightnessSlider() {
        changeBriBridge()
        
        var ligts : [String: Any] = [:]
        
        for i in 0..<self.rBridge.lights.count {
            ligts[self.rBridge.lights[i].id] = [
                "state" : [
                    "brightness" : self.rBridge.lights[i].bri
                ]
            ]
        }
        
        let dic = [
            "state" : [
                "desired" : [
                    //"name" : self.bridge.name,
                    "lights" :  ligts
                ]
            ]
        ]
        
        shadowUpdate(dic: dic)
    }
    
}
