//
//  Constant.swift
//  iosProject
//
//  Created by Karim HAMDI on 07/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import Foundation
import UIKit
import AWSCore
import SQLite


let CertificateSigningRequestCommonName = "iOSProject Application"
let CertificateSigningRequestCountryName = "Ireland" //A modifier en cas de changement de region
let CertificateSigningRequestOrganizationName = "UTT"
let CertificateSigningRequestOrganizationalUnitName = "ISI"
let PolicyName = "mysubpolicy" //A modifier en fonction du nom de la Stratégie créée

let AWSRegion = AWSRegionType.EUWest1 // e.g. AWSRegionType.USEast1 //A modifier en cas de changement de region
let IOT_ENDPOINT = "https://a2925nglmmp0xz-ats.iot.eu-west-1.amazonaws.com" // A modifier avec le nouveau EndPoint générer par AWS
let ASWIoTDataManager = "MyIotDataManager"

//Coordonée (x,y) de la couleur blanche dans le cercle de couleur.
let XWHITE = 0.3146
let YWHITE = 0.3303

let whiteC = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)

//Base de donnée
var db: Connection!

//Table Groupes
let groupe_Table = Table("groupes")
let groupe_id = Expression<String>("id_groupe")
let groupe_name = Expression<String>("groupe_name")


//Table Light
let light_Table = Table("lights")
let light_id = Expression<Int>("id_light")
let light_name = Expression<String>("light_name")

//Table Groups Lights
let groupeLights_Table = Table("groupsLight")
let groupeLights_id_groupe = Expression<String>("id_groupe")
let groupeLights_id_light = Expression<Int>("id_light")


var firstGroup = false
var firstMode = false




class Lights{
    let id: String
    let name: String
    var bri: Int
    var on: Bool
    var xy: (Double, Double)!
    
    var reachable: Bool
    
    var color: Bool
    var effect: String!
    var colorTemp: Int
    
    
    
    init(id: String, name: String, bri: Int, on: Int, xy: (Double, Double), color: String, reachable: Int, colorTemp: Int, effect: String ) {
        self.id = id
        self.name = name
        self.bri = bri
        self.on = Lights.getBool(nb: on)
        self.xy = xy
        
        self.color = Lights.getColorType(str: color)
        
        self.reachable =  Lights.getBool(nb: reachable)
        self.colorTemp = colorTemp
        self.effect = effect
    }
    
    init(id: String, name: String, bri: Int, on: Int, color: String, reachable: Int, colorTemp: Int ) {
        self.id = id
        self.name = name
        self.bri = bri
        self.on = Lights.getBool(nb: on)
        
        self.color = Lights.getColorType(str: color)
        
        self.reachable =  Lights.getBool(nb: reachable)
        self.colorTemp = colorTemp
    }
    
    private static func getBool (nb: Int) -> Bool{
        if (nb == 0) {
            return false
        }
        return true
    }
    
    private static func getColorType (str: String) -> Bool{
        if (str == "Extended color light") {
            return true
        }
        return false
    }
    
}

class Bridge {
    
    let name : String
    
    let lights: [Lights]
    
    init(name: String, lights: [ Lights] ) {
        self.name = name
        self.lights = lights
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
