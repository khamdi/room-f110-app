//
//  SingleLampViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 14/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT

class SingleLampViewController: UIViewController {
    
    var indeX: String!
    var isOn: Bool!
    var bri: Float!
    var xy: (Double, Double)!
    var lampName: String!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchLamp: UISwitch!
    @IBOutlet weak var briSlider: UISlider!
    
    var selectedColor: UIColor = UIColor.white
    
    @IBOutlet var colorPicker: SwiftHSVColorPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
     
        
        self.colorPicker.setViewColor(self.selectedColor)

        self.switchLamp.isOn = self.isOn
        self.briSlider.value = self.bri
        self.titleLabel.text = self.lampName
        

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func colorSelected(_ sender: UIButton) {
    
        let point = self.colorPicker.colorWheel.point
        var x: Double, y: Double
        
        
        
        print("x: \(((point?.x)!)/255), y: \((point?.y)!/255)")
        
        print(self.colorPicker.color)
        if (self.colorPicker.color == whiteC) {
            x = XWHITE
            y = YWHITE
        } else {
            self.xy = ( Double((point?.x)!/280), Double((point?.y)!/280) )
            x = ( (10000 * self.xy!.0).rounded() ) / 10000
            y = ( (10000 * self.xy!.1).rounded() ) / 10000
        }
        
        let dic = [
            "state" : [
                "desired" : [
                    //"name" : self.bridge.name,
                    "lights" : [
                        self.indeX : [
                            "state" : [
                                "xy" : [
                                    x,
                                    y
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        
        shadowUpdate(dic: dic)
        
    }
    
    
    
    
    @IBAction func switchOn() {
        if (self.switchLamp.isOn){
            self.briSlider.isEnabled = true
            self.briSlider.value = self.bri
        }
        else {
            self.briSlider.isEnabled = false
            self.briSlider.value = 0
        }
        
        
        let dic = [
            "state" : [
                "desired" : [
                    //"name" : self.bridge.name,
                    "lights" : [
                        self.indeX : [
                            "state" : [
                                "on" : self.switchLamp.isOn
                            ]
                        ]
                    ]
                ]
            ]
        ]
        
        shadowUpdate(dic: dic)
        
    }
    
    @IBAction func briSliderAction() {
        
        self.bri = self.briSlider.value
        
        let dic = [
            "state" : [
                "desired" : [
                    //"name" : self.bridge.name,
                    "lights" : [
                        self.indeX : [
                            "state" : [
                                "brightness" : self.briSlider.value.rounded()
                            ]
                        ]
                    ]
                ]
            ]
        ]
        
        shadowUpdate(dic: dic)
    }
}
