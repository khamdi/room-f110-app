//
//  GroupeViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 18/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT
import SQLite

class GroupeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControlScene: UISegmentedControl!
        
    var groups: [(String, String, [String])] = []
    
    
    let identifiantModuleCellule = "groupsid"
    //var group : [String: Any] = [:]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = self.tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule, for: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifiantModuleCellule)
        }
        cell!.textLabel?.text = "\(self.groups[indexPath.row].1)"
        
        cell?.accessoryType = .detailButton
        var str = "Lights :"
        for i in 0..<self.groups[indexPath.row].2.count {
            str += " \(self.groups[indexPath.row].2[i]) -"
        }
        
        
        print (str)
        cell!.detailTextLabel?.text = String(str.dropLast())
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt
        indexPath: IndexPath) {
        print("You selected the cell, \(indexPath.row)")
        
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print(indexPath.row)
        
        
        
        let alert = UIAlertController(title: "Voulez-vous supprmier ce mode ?", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let oneAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
            print(self.groups[indexPath.row].0)
            iotDataManager.publishString(self.groups[indexPath.row].0, onTopic: "things/PhilipsHueBridge/features/groups/delete", qoS:.messageDeliveryAttemptedAtMostOnce)
            do {
                let row = groupe_Table.filter(groupe_id == self.groups[indexPath.row].0)
                try db.run (row.delete())
                
                let rowG = groupeLights_Table.filter(groupeLights_id_groupe == self.groups[indexPath.row].0)
                try db.run(rowG.delete())
            } catch {
                print (error)
            }
            self.groups.remove(at: indexPath.row)
            self.tableView.reloadData()
        })
        
        let twoAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        alert.addAction(oneAction)
        alert.addAction(twoAction)
        
        
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Groups : "
    }
    
 
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("test")
        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.segmentedControlScene.selectedSegmentIndex = 0
    }
    
    
    

    override func viewDidAppear(_ animated: Bool) {
        //self.tableView.reloadData()
        groupsUpdate()
    }
    
    func groupsUpdate() {
        groupsGetSubscribe()
        groupsGetPublish()
    }
    
    func groupsGetPublish(){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "things/PhilipsHueBridge/features/groups/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func groupsGetSubscribe() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/groups/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    for i in dictio {
                        guard let id = i.value as? [String: Any] else {return}
                        guard let name = id["name"] as? String else {return}
                        guard let lights = id["lightIds"] as? [String] else {return}
                        self.groups.append((i.key, name, lights))
                        
                        self.tableView.reloadData()
                    }
                    self.insertGroups()
                } catch {
                    print(error)
                }
            }
        })
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/groups/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    func getLights() -> [String] {
        var res: [String] = []
        
        for group in groups {
            
            for light in group.2 {
                
                if (!res.contains(light)){
                    res.append(light)
                }
            }
        }
        
        return res
    }
    
    func insertGroups (){        
        if firstGroup == false {
            firstGroup = true
            let lights = getLights()
            do {
                for light in lights {
                    try db.run(light_Table.insert(light_id <- Int(light)!, light_name <- ("Light " + light)))
                }
                
                for group in groups {
                        //add in Groupe Table
                            try db.run(groupe_Table.insert(groupe_id <- group.0, groupe_name <- group.1))
                        
                        //Add Lights
                    
                    for tmp in group.2 {
                        //Add GroupeLight
                        try db.run(groupeLights_Table.insert(groupeLights_id_groupe <- group.0, groupeLights_id_light <- Int(tmp)!))
                    }
                }
            } catch {
                print(error)
            }
                //let insert = groupe_Table.insert()
        }
        else {

            
            let last = getLastGroup()
            
            do {
                try db.run(groupe_Table.insert(groupe_id <- last.0, groupe_name <- last.1))
                
                for j in 0..<last.2.count {
                    
                    //Add GroupeLight
                    try db.run(groupeLights_Table.insert(groupeLights_id_groupe <- last.0, groupeLights_id_light <- Int(last.2[j])!))
                }
                
            } catch {
                print(error)
            }
        }
    }
    
    func getLastGroup () -> (String, String, [String]){
        
        if groups.count != 0 {
            var i = groups[0]
            for group in groups {
                if (Int(group.0)! > Int(i.0)! ) {
                    i = group
                }
            }
            return i
        }
        return ("", "", [])
    }
    
    @IBAction func afficheBD() {
        do {
            
            print("-------Table Groupe-------")
            for group in try db.prepare(groupe_Table) {
                print ("-- Groupe_id : \(group[groupe_id]), Name : \(group[groupe_name]) --")
            }
            print("-------------------------")
            
            print("-------Table Light-------")
            for light in try db.prepare(light_Table) {
                print ("-- Light_id : \(light[light_id]), Name : \(light[light_name]) --")
            }
            print("-------------------------")
            
            print("-------Table Groupe Light-------")
            for group in try db.prepare(groupeLights_Table) {
                //print ("id : \(group[groupeLights_id]), Groupe_id : \(group[groupeLights_id_groupe]), Light_id \(group[groupeLights_id_light])")
                print (" Groupe_id : \(group[groupeLights_id_groupe]), Light_id \(group[groupeLights_id_light])")
            }
            print("--------------------------------")
            
        } catch {
            print(error)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
