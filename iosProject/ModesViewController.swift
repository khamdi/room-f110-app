//
//  ModesViewController.swift
//  iosProject
//
//  Created by Karim HAMDI on 18/01/2019.
//  Copyright © 2019 Karim HAMDI. All rights reserved.
//

import UIKit
import AWSIoT

class ModesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var addMode: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lauchMode: UIButton!
    @IBOutlet weak var segmentedControlScene: UISegmentedControl!
    
    let identifiantModuleCellule = "modesid"
    
    var selectedID: String!
    var modes: [(String, String, [String])] = []
    
    /*required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        selectionStyle = .none
    }*/
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = self.tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule, for: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: identifiantModuleCellule)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifiantModuleCellule)
        }
        cell!.textLabel?.text = "\(self.modes[indexPath.row].1)"
        
        cell?.accessoryType = .detailButton
        var str = "Lights :"
        for i in 0..<self.modes[indexPath.row].2.count {
            str += " \(self.modes[indexPath.row].2[i]) -"
        }
        
        
        print (str)
        cell!.detailTextLabel?.text = String(str.dropLast())
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print(indexPath.row)
        
        
        
        let alert = UIAlertController(title: "Voulez-vous supprmier ce mode ?", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let oneAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
            print(self.modes[indexPath.row].0)
            iotDataManager.publishString(self.modes[indexPath.row].0, onTopic: "things/PhilipsHueBridge/features/scenes/delete", qoS:.messageDeliveryAttemptedAtMostOnce)
            self.modes.remove(at: indexPath.row)
            self.tableView.reloadData()
            
        })
        
        let twoAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        alert.addAction(oneAction)
        alert.addAction(twoAction)
        
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
        self.lauchMode.isEnabled = true
        self.selectedID = self.modes[indexPath.row].0
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .detailButton
        self.lauchMode.isEnabled = false
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Modes : "
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        modesUpdate()
        self.lauchMode.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.segmentedControlScene.selectedSegmentIndex = 1
    }
    
    @IBAction func launchModes() {
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        print("Selected ID : \(self.selectedID)")
        iotDataManager.publishString(self.selectedID, onTopic: "things/PhilipsHueBridge/features/scenes/set", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    
    func modesUpdate() {
        modesGetSubscribe()
        modesGetPublish()
    }
    
    func modesGetPublish(){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.publishString("", onTopic: "things/PhilipsHueBridge/features/scenes/get", qoS:.messageDeliveryAttemptedAtMostOnce)
    }
    
    func modesGetSubscribe(){
        let iotDataManager = AWSIoTDataManager(forKey: ASWIoTDataManager)
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/scenes/get/accepted", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("accecpted received: \(stringValue)")
            
            DispatchQueue.main.async {
                do {
                    let jsonString = stringValue.data(using: String.Encoding.utf8.rawValue)
                    let dic = try JSONSerialization.jsonObject(with: jsonString!, options: .mutableLeaves)
                    
                    print("dic \(dic)")
                    
                    
                    //  if let dictio = dic as? [String: Any] {}
                    guard let dictio = dic as? [String: Any] else {return}
                    
                    for i in dictio {
                        guard let id = i.value as? [String: Any] else {return}
                        guard let name = id["name"] as? String else {return}
                        guard let lights = id["lightIds"] as? [String] else {return}
                        self.modes.append((i.key, name, lights))
                        
                        self.tableView.reloadData()
                    }
                } catch {
                    print(error)
                }
            }
        })
        
        iotDataManager.subscribe(toTopic: "things/PhilipsHueBridge/features/scenes/get/rejected", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
            (payload) ->Void in
            
            let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
            
            print("rejected received: \(stringValue)")
        } )
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
